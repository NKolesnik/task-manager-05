package ru.t1consulting.nkolesnik.tm;

import ru.t1consulting.nkolesnik.tm.constant.TerminalConst;

public final class Application {

    public static void main(final String[] args) {
        process(args);
    }

    public static void process(final String[] args) {
        if (args == null || args.length < 1) {
            return;
        }
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) {
            return;
        }
        switch (arg) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikolay Kolesnik");
        System.out.println("E-mail: kolesnik.nik.vrn@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show developer info.\n", TerminalConst.ABOUT);
        System.out.printf("%s - Show application version.\n", TerminalConst.VERSION);
        System.out.printf("%s - Show terminal commands.\n", TerminalConst.HELP);
    }

    public static void showError(final String arg) {
        System.err.printf("Error! This argument `%s` is not supported\n", arg);
    }

}
